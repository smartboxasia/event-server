'use strict';

var dsCall = require('./dsConnector');

module.exports = {

  callScene: function (sceneNumber, next) {
    dsCall(`/json/apartment/callScene?sceneNumber=${sceneNumber}&force=true`,
      function (err, json) {
        if (err) { return next(err); }
        if (!json.ok) { return next(new Error(json.message)); }
        next();
      });
  },

  undoScene: function (sceneNumber, next) {
    dsCall(`/json/apartment/undoScene?sceneNumber=${sceneNumber}`,
      function (err, json) {
        if (err) { return next(err); }
        if (!json.ok) { return next(new Error(json.message)); }
        next();
      });
  },

  getStructure: function (next) {
    dsCall('/json/apartment/getStructure',
      function (err, json) {
        if (err) { return next(err); }
        if (!json.ok) { return next(new Error(json.message)); }
        var result = json.result;
        result.apartment.zones = result.apartment.zones
                                    .filter(function(device) {
                                      return device.isPresent; // filter absent zones
                                    });
        next(null, result);
      });
  },

  getCircuits: function (next) {
    dsCall('/json/apartment/getCircuits', function (err, json) {
      if (err) { return next(err); }
      if (!json.ok) { return next(new Error(json.message)); }
      var result = json.result;
      result.circuits = result.circuits
                          .filter(function(circuit) {
                            return circuit.isPresent; // filter absent circuit
                          });
      next(null, result);
    });
  },

};
