'use strict';

var dsCall = require('./dsConnector');

module.exports = {

  getConsumption: function (id, next) {
    dsCall(`/json/circuit/getConsumption?id=${id}`,
      function (err, json) {
        if (err) { return next(err); }
        if (!json.ok) { return next(new Error(json.message)); }
        next(null, json.result);
      });
  },

};
