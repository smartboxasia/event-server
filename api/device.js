'use strict';

var dsCall = require('./dsConnector');

module.exports = {

  getOutputValue: function (dsid, next) {
    dsCall(`/json/device/getOutputValue?offset=0&dsid=${dsid}`,
      function (err, json) {
        if (err) { return next(err); }
        if (!json.ok) { return next(new Error(json.message)); }
        next(null, json.result);
      });
  },

  setValue: function (dsid, value, next) {
    dsCall(`/json/device/setValue?dsid=${dsid}&value=${value}`,
      function (err, json) {
        if (err) { return next(err); }
        if (!json.ok) { return next(new Error(json.message)); }
        next(null, value);
      });
  },

};
