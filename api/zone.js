'use strict';

var dsCall = require('./dsConnector');

module.exports = {

  callScene: function(id, groupID, sceneNumber, next) {
    var href = `/json/zone/callScene?id=${id}&sceneNumber=${sceneNumber}`;
    if (groupID) { href += `&groupID=${groupID}`; }
    dsCall(href, function(err, json) {
      if (err) { return next(err); }
      if (!json.ok) { return next(new Error(json.message)); }
      next();
    });
  },

  sceneGetName: function(id, groupID, sceneNumber, next) {
    dsCall(`/json/zone/sceneGetName?id=${id}&groupID=${groupID}&sceneNumber=${sceneNumber}`,
      function(err, json) {
        if (err) { return next(err); }
        if (!json.ok) { return next(new Error(json.message)); }
        next(null, json.result);
      });
  },

};
