'use strict';

/**
 * Trigger URL to dS server and
 * Ensure connectivity by get/refresh token automatically
 */

var async    = require('async');
var https    = require('https');
var config   = require('../config/config');

var isDevelopment = !process.env.NODE_ENV ? true : process.env.NODE_ENV === 'development';

var ip       = config.ds.ip;
var port     = config.ds.port;
var appToken = config.ds.appToken;
var   token;
var   lastAccess = 0;


function _httpsGet(url, next) {
  if (isDevelopment) { console.log(`  [dsConnector] ${url}`); }

  https
    .get(url, function (res) {
      var data = '';
      res
        .on('data', function (d) { data += d; })
        .on('end', function() {
          var json = JSON.parse(data) || '';
          if (!json.ok) {
            return next(new Error(`DS Server Response ${res.statusCode}: ${json.message}`));
          }
          if (isDevelopment) { console.log(`  [dsConnector] ${JSON.stringify(json)}`); }
          next(null, json);
        })
        .on('error', next);
    })
    .on('error', next)
    .on('socket', function (socket) {
      socket.setTimeout(config.httpTimeout);
      socket.on('timeout', function () {
        if (isDevelopment) { console.log(`  [dsConnector] [TIMEOUT] ${url}`); }
        next(new Error(`request timeout: ${url}`));
      });
    });
}

function _isTokenValid(next) {
  // bypass http query if token haven't expire
  if (lastAccess > new Date().valueOf() - 60000) {
    console.log(`  [dsConnector] SKIP http token check`);
    return next(null, true);
  }

  console.log(`  [dsConnector] http token check`);
  var url = `https://${ip}:${port}/json/apartment/getName?token=${token}`;
  _httpsGet(url, function (err) { next(null, !err); });
}

function _refreshToken(next) {
  var url = `https://${ip}:${port}/json/system/loginApplication?loginToken=${appToken}`;
  _httpsGet(url, function (err, json) {
    if (err) { return next(err); }
    token = json.result.token;
    next();
  });
}

function _successCallUpdateTime(json, next) {
  lastAccess = new Date().valueOf();
  next(null, json);
}


module.exports = function (href, next) {
  async.waterfall([

    _isTokenValid,

    function (valid, next) {
      if (valid) { return next(); }
      _refreshToken(next);
    },

    function (next) {
      var symbol = href.indexOf('?') === -1 ? '?' : '&' ;
      var url = `https://${ip}:${port}${href}${symbol}token=${token}`;
      _httpsGet(url, next);
    },

    _successCallUpdateTime

  ], next);
};
