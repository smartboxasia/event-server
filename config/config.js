'use strict';

var path = require('path');
var IP   = require('ip');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'; // to accept unsigned certificate

/**
 * Jakarta Event Server MUST BE this
 * things that are affected by the ip:
 * 1. Event Responder
 * 2. Door Bird
 */
// var ip   = IP.address();
var ip   = '192.168.0.106';

var port = process.env.PORT || '3000';


module.exports = {

  root: path.normalize(__dirname + '/..'),

  host: `http://${ip}:${port}`,
  port: port,
  httpTimeout: 30 * 1000,

  isAway: false,
  isWindowOpen: false,

  // /* Event */
  ds: {
    ip       : '192.168.0.101',
    port     : 8080,
    appToken : '1b0d03b61e86199d68bc731fee7d016d8a93071d57e90bf453d228d37085b63b'
  },

  // /* Development */
  // ds: {
  //   ip       : 'dss-dev.lan',
  //   port     : 8080,
  //   appToken : '9b8aec5be3c659cbaa985fd714fe6a1db346c4a4163aba717f657a84fb842169'
  // },

  // /* Show Room */
  // ds: {
  //   ip       : '58f117548b894aafa2ff5437fc50ad4e.digitalstrom.net',
  //   port     : 8080,
  //   appToken : '36bcca29f1664c040d07c3a0cfd15c974adb30167fb10e54974b9876fa72febc'
  // },

  ipcam: {
    ip: '192.168.0.100'
  },

  /**
   * Door Bird Hook Registration:
   * http://192.168.0.103/bha-api/notification.cgi?reset=1
   * http://192.168.0.103/bha-api/notification.cgi?event=doorbell&subscribe=1&url=http://192.168.0.106/api/notify/doorbird/bell
   * http://192.168.0.103/bha-api/notification.cgi?event=doorbell&subscribe=1&url=http://192.168.0.102:3000/api/notify/doorbird/bell
   */
  doorbird: {
    ip: '192.168.0.103',
    user: 'ggysyn0001',
    pass: 'snhtkmmxmq'
  },

};
