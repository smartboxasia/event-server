'use strict';

module.exports = function () {
  var topics = {};

  return {
    subscribe: function (topic, listener) {
      if (!topics[topic]) {
        topics[topic] = [];
      }
      topics[topic].push(listener);
    },

    publish: function (topic, data) {
      var listeners = topics[topic];
      if (!listeners || listeners.length < 1) {
        return;
      }

      listeners.forEach(function (listener) {
        try {
          listener(data);
        } catch (e) {
          console.error(e);
        }
      });
    }
  };
};
