'use strict';

var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var express      = require('express');
var favicon      = require('serve-favicon');
var glob         = require('glob');
var logger       = require('morgan');
var app          = express();

module.exports = function(config, bus) {

  // view engine setup
  app.set('views', `${config.root}/views`);
  app.set('view engine', 'pug');

  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());

  app.use(favicon(`${config.root}/public/images/favicon.png`));
  app.use(express.static(`${config.root}/public`));
  app.use('/bower_components', express.static(`${config.root}/bower_components`));


  /**
   * Routers
   */
  var routers = glob.sync(`${config.root}/routers/*.js`);
        routers.forEach(function (path) {
          require(path)(app, config, bus);
        });


  // catch 404 and forward to error handler
  app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // error handlers

  // development error handler
  // will print stacktrace
  if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', { message: err.message, error: err });
    });
  }

  // production error handler
  // no stacktraces leaked to user
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', { message: err.message, error: {} });
  });


  return app;
};
