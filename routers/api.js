'use strict';

var glob   = require('glob');
var router = require('express').Router();

module.exports = function (app, config, bus) {

  var apis = glob.sync(`${config.root}/routers/api/*.js`);
        apis.forEach(function (path) {
          require(path)(router, config, bus);
        });

  app.use('/api', router);

};
