'use strict';

var router = require('express').Router();

module.exports = function (parentRouter, config, bus) {
  parentRouter.use('/notify', router);

  var device = require(`${config.root}/api/device`);

  // triggered digitalSTROM event responder
  router.get('/window/:status', (req, res) => {
    var status = req.params.status;
    config.isWindowOpen = status !== 'close';

    console.log(`  !!! WINDOW: ${config.isWindowOpen}, AWAY: ${config.isAway}`)
    if (!config.isWindowOpen) {
      // turn off alarm
      console.log('  !!! [ALARM OFF] turn off alarm');
      device.setValue('302ed89f43f00ec0000a03a6', 0, (err) => {});
    }
    if (config.isWindowOpen && config.isAway) {
      // alarm!
      console.log('  !!! [ALARM ON] you are AWAY and window is OPEN');
      bus.publish('bus-alarm', { alarm: true });
      device.setValue('302ed89f43f00ec0000a03a6', 255, (err) => {});
    }

    res.json({ ok: true, result: { window: { status } } });
  });

  // triggered by Door Bird
  router.get('/doorbird/:action', (req, res) => {
    var action = req.params.action;
    var json = { doorbird: { action } };
    if (action === 'bell') {
      bus.publish('bus-doorbird', json);
    }
    res.json({ ok: true, result: req.params.action });
  });

  router.get('/switch/:zoneId/groupId/:sceneNumber', (req, res) => {
    var zoneId = req.params.zoneId;
    var groupId = req.params.groupId;
    var sceneNumber = req.params.sceneNumber;

    if (groupId === 1) {
      bus.publish('bus-update-light', { zoneId, sceneNumber });
    }

  });

  router.get('/:call', (req, res) => {
    console.log('notify:', req.params.call);
    res.json({ ok: true, result: req.params.call });
  });

};
