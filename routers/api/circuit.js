'use strict';

var router = require('express').Router();

module.exports = function (parentRouter, config) {
  parentRouter.use('/circuit', router);

  var circuit = require(`${config.root}/api/circuit`);

  router.get('/:dsid/getConsumption', (req, res) => {
    circuit.getConsumption(req.params.dsid,
      (err, result) => {
        if (err) { return res.json({ ok: false, message: err.message }); }
        res.json({ ok: true, result});
      });
  });

};
