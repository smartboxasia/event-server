'use strict';

var router = require('express').Router();

module.exports = function (parentRouter, config, bus) {
  parentRouter.use('/apartment', router);

  var apartment = require(`${config.root}/api/apartment`);

  router.get('/getState', function (req, res) {
    var state = config.isAway ? 'away' : 'home';
    res.json({ ok: true, result: { apartment: { state } } });
  });

  router.get('/setState/:state', function (req, res) {
    if (req.params.state === 'home') {

      apartment.callScene(71, function (err) {
        if (err) { return res.json({ ok: false, message: err.message }); }
        config.isAway = false;
        bus.publish('bus-update-home', { apartment: { state: 'home' } });
        res.json({ ok: true });
      });

    } else if (req.params.state === 'away') {

      // validate if window still open
      if (config.isWindowOpen) {
        return res.json({ ok: true, windowOpen: true });
      }

      apartment.callScene(72, function (err) {
        if (err) { return res.json({ ok: false, message: err.message }); }
        config.isAway = true;
        bus.publish('bus-update-home', { apartment: { state: 'away' } });
        res.json({ ok: true });
      });

    } else {
      res.json({ ok: false, message: 'Invalid state value (home/away)'});
    }
  });

  router.get('/callScene/:sceneNumber', function (req, res) {
    apartment.callScene(sceneNumber, function (err) {
      if (err) { return res.json({ ok: false, message: err.message }); }
      res.json({ ok: true });
    });
  });

  router.get('/undoScene/:sceneNumber', function (req, res) {
    apartment.undoScene(req.params.sceneNumber, function (err) {
      if (err) { return res.json({ ok: false, message: err.message }); }
      res.json({ ok: true });
    });
  });

  // TODO provide another layer of caching devices
  router.get('/getStructure', function (req, res) {
    apartment.getStructure(function(err, result) {
      if (err) { return res.json({ ok: false, message: err.message }); }
      res.json({ ok: true, result });
    });
  });

  // TODO provide another layer of caching devices
  router.get('/getCircuits', function (req, res) {
    apartment.getCircuits(function(err, result) {
      if (err) { return res.json({ ok: false, message: err.message }); }
      res.json({ ok: true, result });
    });
  });

};
