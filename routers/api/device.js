'use strict';

var router = require('express').Router();

module.exports = function (parentRouter, config) {
  parentRouter.use('/device', router);

  var device = require(`${config.root}/api/device`);

  router.get('/:dsid/getOutputPercentage', (req, res) => {
    device.getOutputValue(req.params.dsid, (err, result) => {
      if (err) { return res.json({ ok: false, message: err.message }); }
      var percentage = result.value / 255;
      res.json({ ok: true, result: { percentage } });
    });
  });

  router.get('/:dsid/setOutputPercentage/:percentage', (req, res) => {
    var dsid  = req.params.dsid;
    var percentage = parseFloat(req.params.percentage);
    var value = Math.round(percentage * 255);

    device.setValue(dsid, value, (err) => {
      if (err) { return res.json({ ok: false, message: err.message }); }
      res.json({ ok: true, result: { percentage } });
    });
  });

};
