'use strict';

var router = require('express').Router();

module.exports = function (parentRouter, config, bus) {
  parentRouter.use('/zone', router);

  var zone = require(`${config.root}/api/zone`);

  router.get('/:id/callScene/:sceneNumber', function(req, res) {
    zone.callScene(req.params.id, null, req.params.sceneNumber, function (err) {
      if (err) { return res.json({ ok: false, message: err.message }); }
      res.json({ ok: true });
    });
  });

  router.get('/:id/group/:groupId/sceneGetName/:sceneNumber', function(req, res) {
    zone.sceneGetName(req.params.id, req.params.groupId, req.params.sceneNumber, function(err, json) {
      if (err) { return res.json({ ok: false, message: err.message }); }
      res.json({ ok: true, result: json });
    });
  });

  router.get('/:zoneId/group/:groupId/callScene/:sceneNumber', function(req, res) {
    var zoneId = req.params.zoneId;
    var groupId = req.params.groupId;
    var sceneNumber = req.params.sceneNumber;
    zone.callScene(zoneId, groupId, sceneNumber, function (err) {
      if (err) { return res.json({ ok: false, message: err.message }); }

      if (groupId === 1) {
        bus.publish('bus-update-light', { zoneId, sceneNumber });
      }

      res.json({ ok: true });
    });
  });

};
