'use strict';

var router = require('express').Router();
var MjpegProxy = require('mjpeg-proxy').MjpegProxy;

module.exports = function (app, config) {
  app.use('/', router);

  var opts = {
    title      : 'SMARTBOX Asia',

    serverHost : config.host,
    ipcamIp    : config.ipcam.ip,
    doorbirdIp : config.doorbird.ip
  };

  router.get('/', function(req, res, next) {
    res.render('index', opts);
  });

  const username = config.doorbird.user;
  const password = config.doorbird.pass;
  const ip       = config.doorbird.ip;
  router.get('/doorbird/video.jpg', new MjpegProxy(`http://${username}:${password}@${ip}/bha-api/video.cgi`).proxyRequest);

  router.get('/old', function(req, res, next) {
    res.render('old', opts);
  });

};
