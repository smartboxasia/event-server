
__get('/api/apartment/getCircuits',
  function(json) {
    var circuits = json.result.circuits;
    var consumption = 0;

    async.each(circuits,
      function(circuit, next) {
        if (!circuit.isPresent) { return next(); }
        var url = '/api/circuit/' + circuit.dsid + '/getConsumption';
        __get(url, function(json) {
          consumption += json.result.consumption;
          next();
        });
      },
      function(err) {
        $('#home-section-consumption').find('.mdl-spinner').first().removeClass('is-active');
        $('#home-section-consumption-value').text(consumption);
      });
},
function(url, msg) {
  $('#home-section-consumption').find('.mdl-spinner').first().removeClass('is-active');
});
