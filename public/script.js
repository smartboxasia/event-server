'use strict';

/* globals io, async, __get, componentHandler */

$(document).ready(function() {

  /**
   * Web Socket
   */
  // var host   = $('#server-host');
  // var socket = io.connect(host);
  //     socket.on('socket-device', function (json) {
  //       $(json.id).text(json.percentage);
  //     });


  /**
   * Door Bird
   */
  // var doorbird = $('#doorbird-ip'); // TODO then what? trigger websocket for all the api call?


  /**
   * Load Apartment
   */


  // function applyApartmentSceneBtn(btnId, sceneNumber) {
  //   $(btnId).click(function(e) {
  //     __get(`/api/apartment/callScene/${sceneNumber}`);
  //   });
  // }
  // applyApartmentSceneBtn(`#btn-absent`  , 72);
  // applyApartmentSceneBtn(`#btn-present` , 71);
  // applyApartmentSceneBtn(`#btn-doorbell`, 73);
  // applyApartmentSceneBtn(`#btn-panic`   , 65);
  // applyApartmentSceneBtn(`#btn-fire`    , 76);
  // applyApartmentSceneBtn(`#btn-alarm1`  , 74);
  // applyApartmentSceneBtn(`#btn-alarm2`  , 83);


  /**
   * Load Zones
   */

  // __get('/api/apartment/getStructure', function(json) {
  //
  //   var zones = json.result.apartment.zones;
  //   zones.forEach(function(zone) {
  //
  //     var zoneId = zone.id;
  //
  //   });
  //
  // });

  // $.get('snippet/light.html', function(data) {
  //   $zone.append(data);
  //   console.log(`zone-${zoneId}`);
  //   componentHandler.upgradeAllRegistered();
  // }, 'text');

  // __get('/api/apartment/getStructure', function(json) {
  //   var $container = $('#zonesContainer');
  //       $container.empty();
  //
  //   var zones = json.result.apartment.zones;
  //   zones.forEach(function(zone) {
  //
  //     // Load Zone Content
  //
  //     var zoneId   = zone.id;
  //     var $zoneDOM = `<div class='panel panel-default'>` +
  //                       `<div class='panel-body'>` +
  //                         `<div>` +
  //                           `<h3 class='text-center'>${zone.name}</h3>` +
  //                           `<div class='text-center'>` +
  //                             `<button id='${zoneId}-standby' class='btn btn-info'>Stand By</button> ` +
  //                             `<button id='${zoneId}-deepoff' class='btn btn-info'>Deep Off</button> ` +
  //                             `<button id='${zoneId}-sleep' class='btn btn-info'>Sleep</button> ` +
  //                             `<button id='${zoneId}-wakeup' class='btn btn-info'>Wake Up</button> ` +
  //                           `</div>` +
  //                           `<br/>` +
  //                         `</div>` +
  //                         `<div id='${zoneId}-group-box'></div>` +
  //                       `</div>` +
  //                     `</div>`;
  //     $container.append($zoneDOM);
  //     var $zoneGroupBox = $container.find(`#${zoneId}-group-box`);
  //
  //     function applyZoneSceneBtn(btnId, zoneId, sceneNumber) {
  //       $(btnId).click(function(e) {
  //         __get(`/api/zone/${zoneId}/callScene/${sceneNumber}`);
  //       });
  //     }
  //     applyZoneSceneBtn(`#${zoneId}-standby`, zoneId, 67);
  //     applyZoneSceneBtn(`#${zoneId}-deepoff`, zoneId, 68);
  //     applyZoneSceneBtn(`#${zoneId}-sleep`  , zoneId, 69);
  //     applyZoneSceneBtn(`#${zoneId}-wakeup` , zoneId, 70);
  //
  //
  //     // Append Zone Groups
  //
  //     var devices = zone.devices;
  //     var groups  = zone.groups;
  //
  //     var lights = groups.find(function(group) { return group.id === 1 && group.devices.length > 0; });
  //     var shade  = groups.find(function(group) { return group.id === 2 && group.devices.length > 0; });
  //     var audio  = groups.find(function(group) { return group.id === 4 && group.devices.length > 0; });
  //     var video  = groups.find(function(group) { return group.id === 5 && group.devices.length > 0; });
  //     var aircon = groups.filter(function(group) { return [3, 9, 10, 11].indexOf(group.id) !== -1 && group.devices.length > 0; });
  //
  //     /**
  //      * Load Lights
  //      */
  //
  //     if (lights) {
  //
  //       // Load Lights Content
  //
  //       var groupId   = 1;
  //       var groupName = 'Lights';
  //       var $groupDOM = `<div class='panel panel-default'>` +
  //                         `<div class='panel-body'>` +
  //                           `<div>` +
  //                             `<h4 class='text-center'>${groupName}</h4>` +
  //                             `<div id='${zoneId}-${groupId}-scene-box' class='text-center'></div>` +
  //                             `<br/>` +
  //                           `</div>` +
  //                           `<div id='${zoneId}-${groupId}-device-box'></div>` +
  //                        `</div>`;
  //       $zoneGroupBox.append($groupDOM);
  //       var $groupSceneBox  = $zoneGroupBox.find(`#${zoneId}-${groupId}-scene-box`);
  //       var $groupDeviceBox = $zoneGroupBox.find(`#${zoneId}-${groupId}-device-box`);
  //
  //
  //       // Get Lights Preset Buttons
  //
  //       function applyGroupSceneBtn(btnId, zoneId, groupId, sceneNumber) {
  //         $(`#${btnId}`).click(function(e) {
  //           __get(`/api/zone/${zoneId}/group/${groupId}/callScene/${sceneNumber}`);
  //         });
  //       }
  //
  //       function getGroupSceneButton(zoneId, groupId, sceneNumber, defaultName) {
  //         __get(`/api/zone/${zoneId}/group/${groupId}/sceneGetName/${sceneNumber}`, function(json) {
  //           if (json.result.name.length > 0) {
  //             $groupSceneBox.append(`&nbsp;<button id='${zoneId}-${groupId}-${sceneNumber}' class='btn btn-warning'>${json.result.name}</button>`);
  //             applyGroupSceneBtn(`${zoneId}-${groupId}-${sceneNumber}`, zoneId, groupId, sceneNumber);
  //             return;
  //           }
  //           /* comment below to hide no named preset */
  //           // $groupSceneBox.append(`&nbsp;<button id='${zoneId}-${groupId}-${sceneNumber}' class='btn btn-warning'>${defaultName}</button>`);
  //           // applyGroupSceneBtn(`${zoneId}-${groupId}-${sceneNumber}`, zoneId, groupId, sceneNumber);
  //         });
  //       }
  //
  //       getGroupSceneButton(zoneId, groupId,  0, 'Off');
  //       getGroupSceneButton(zoneId, groupId,  5, 'Preset 1');
  //       getGroupSceneButton(zoneId, groupId, 17, 'Preset 2');
  //       getGroupSceneButton(zoneId, groupId, 18, 'Preset 3');
  //       getGroupSceneButton(zoneId, groupId, 19, 'Preset 4');
  //
  //       // Append Light Devices
  //
  //       lights.devices.forEach(function(dSUID) {
  //         var device = devices.find(function(device) { return device.dSUID === dSUID; });
  //
  //         var dsid = device.id;
  //         var dsPercentageId = `#device-${dsid}-percentage`;
  //
  //         // Load Device Percentages
  //         __get(`/api/device/${dsid}/getOutputPercentage`, function(json) {
  //           $(dsPercentageId).text(json.result.percentage);
  //         });
  //
  //         // Append Light Device DOM
  //         var $col = `<div>` +
  //                      `<span><b>${device.name}</b></span>` +
  //                      `&nbsp;&nbsp;&nbsp;` +
  //                      `<span id='device-${dsid}-percentage'>0</span>%` +
  //                      `&nbsp;&nbsp;&nbsp;` +
  //                      `<span><button id='device-${dsid}' class='btn btn-sm btn-default'>Toggle</button></span>` +
  //                    `</div>`;
  //         $groupDeviceBox.append($col);
  //
  //         $(`#device-${dsid}`).click(function(e) {
  //           var percentage = $(dsPercentageId).text();
  //               percentage = parseInt(percentage, 10);
  //               percentage = percentage > 0 ? 0 : 100;
  //
  //           __get(`/api/device/${dsid}/setOutputPercentage/${percentage}`, function(json) {
  //             socket.emit('socket-device', { id: dsPercentageId, percentage });
  //           });
  //         });
  //
  //       });
  //     }
  //
  //
  //     /**
  //      * Load Shades
  //      */
  //
  //     if (shade) {
  //
  //     }
  //
  //   });
  //
  // });

});
