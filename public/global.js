'use strict';

/* globals noty */

function __noty(text) {
  return;
  // if (text) {
  //   noty({
  //     text,
  //     layout: 'bottomRight',
  //     type: 'error',
  //     timeout: 3000,
  //     maxVisible: 3
  //   });
  // }
}

// JQuery GET function with standard error handling
function __get(url, callback, errorCallback) {
  $.get(url, function(json) {
    if (!json.ok) {
      console.error(`JSON-NOT-OK ${url} ${json.message}`);
      __noty(json.message);
      if (errorCallback) {
        errorCallback(url, json.message);
      }
      return;
    }
    if (callback) {
      callback(json);
    }
  })
  .fail(function() {
    console.error(`FAIL ${url}`, arguments);
    __noty('error');
    if (errorCallback) {
      errorCallback(url, 'FAIL');
    }
  });
}
