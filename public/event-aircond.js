'use strict';


function initAircond(sectionId) {

  var stepRate = 100 / (30 - 16);
  function _getSteps(value) {
    return value - 16 * stepRate;
  }

  function _getValue(step) {
    return Math.round(step / stepRate * 100) + 16;
  }

  function _setSwitchUI($switchLabel, $switch, on) {
    if (on) {
      $switchLabel.addClass('is-checked');
      $switch.prop('checked', true);
    } else {
      $switchLabel.removeClass('is-checked');
      $switch.prop('checked', false);
    }
  }

  function _setSliderUI($slider, $sliderLower, $sliderUpper, value) {
    if (value > 0) {
      $slider.removeClass('is-lowest-value');
    } else {
      $slider.addClass('is-lowest-value');
    }

    var step = _getSteps(value);
    var inverse = 1 - step;

    $slider.val(value);
    $sliderLower.css('flex-grow', step);
    $sliderUpper.css('flex-grow', inverse);
  }

  function _setValue($value, value) {
    $value.text(value);
  }

  var $section = $(`#${sectionId}`);
  var $value   = $section.find(`#${sectionId}-value`);
  var $switchLabel = $section.find('label.mdl-switch.mdl-js-switch').first();
  var $switch = $switchLabel.children().first();
  var $slider = $section.find('input.mdl-slider.mdl-js-slider').first();
  var $sliderLower = $slider.next().children().first(); // .mdl-slider__background-lower
  var $sliderUpper = $sliderLower.next();               // .mdl-slider__background-upper

  var value = 16;
  _setSwitchUI($switchLabel, $switch, value > 0);
  _setSliderUI($slider, $sliderLower, $sliderUpper, value);
  _setValue($value, value);

  var currentStep = _getSteps(value);

  // Input Listener, sync slider with value
  $slider.on('change mousemove', function() {
    var step = parseFloat($sliderLower.css('flex-grow'));
    if (currentStep !== step) {
      currentStep = step;
      _setValue($value, _getValue(step));
    }
  });

}
