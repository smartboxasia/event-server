'use strict';

/* globals __get */

function initLight(sectionId, zoneId, deviceId) {

  var _percentage2step = {
      '0': 0,
      '5': 0.25,
     '25': 0.5,
     '50': 0.75,
    '100': 1
  };
  function percentage2step(percentage) {
    percentage = Math.round(percentage * 100);
    return _percentage2step[percentage];
  }

  var step2number = {
    '0'   :  0,
    '0.25': 19,
    '0.5' : 18,
    '0.75': 17,
    '1'   :  5
  };
  var number2step = {
     '0': 0,
    '19': 0.25,
    '18': 0.5,
    '17': 0.75,
     '5': 1
  };

  function _setSwitchUI($switchLabel, $switch, isOn) {
    if (isOn) {
      $switchLabel.addClass('is-checked');
    } else {
      $switchLabel.removeClass('is-checked');
    }

    $switch.prop('checked', isOn);
  }

  function _setSliderUI($slider, $sliderLower, $sliderUpper, step) {
    if (step === 0) {
      $slider.addClass('is-lowest-value');
    } else {
      $slider.removeClass('is-lowest-value');
    }

    $slider.val(step);
    $sliderLower.css('flex-grow', step);
    $sliderUpper.css('flex-grow', 1 - step);
  }

  var $section = $(`#${sectionId}`);
  var $spinner = $section.find('div.mdl-spinner').first();
  var $switchLabel = $section.find('label.mdl-switch').first();
  var $switch = $switchLabel.children().first();
  var $slider = $section.find('input.mdl-slider').first();
  var $sliderLower = $slider.next().children().first(); // .mdl-slider__background-lower
  var $sliderUpper = $sliderLower.next();               // .mdl-slider__background-upper

  // Get Initial Percentage from Server
  __get(`/api/device/${deviceId}/getOutputPercentage`,
    function(json) {
      $spinner.removeClass('is-active');

      var percentage = json.result.percentage;
      var step = percentage2step(percentage);
      console.log('[device-getOutputPercentage]', step, typeof step);
      _setSwitchUI($switchLabel, $switch, step > 0);
      _setSliderUI($slider, $sliderLower, $sliderUpper, step);

      var current = step;

      // Input Listener, sync switch and slider
      $switchLabel.on('mouseup', function() {
        var step = $switchLabel.hasClass('is-checked') ? 0 : 1;
        var sceneNumber = step2number[step];
        _setSliderUI($slider, $sliderLower, $sliderUpper, step);
        __get(`/api/zone/${zoneId}/group/1/callScene/${sceneNumber}`);
        console.log(`light: ${step}`);
      });

      $slider.on('change mousemove', function() {
        var step = $sliderLower.css('flex-grow');
        if (current !== step) {
          current = step;
          var sceneNumber = step2number[step];
          _setSwitchUI($switchLabel, $switch, step > 0);
          __get(`/api/zone/${zoneId}/group/1/callScene/${sceneNumber}`);
          console.log(`light: ${step}`);
        }
      });
    },
    function(url, msg) {
      $spinner.removeClass('is-active');
    });

}

function syncLight(sectionId, sceneNumber) {

  /* Copy Paste Internal Function from above */

  var number2step = {
     '0': 0,
    '19': 0.25,
    '18': 0.5,
    '17': 0.75,
     '5': 1
  };

  function _setSwitchUI($switchLabel, $switch, isOn) {
    if (isOn) {
      $switchLabel.addClass('is-checked');
    } else {
      $switchLabel.removeClass('is-checked');
    }

    $switch.prop('checked', isOn);
  }

  function _setSliderUI($slider, $sliderLower, $sliderUpper, step) {
    if (step === 0) {
      $slider.addClass('is-lowest-value');
    } else {
      $slider.removeClass('is-lowest-value');
    }

    $slider.val(step);
    $sliderLower.css('flex-grow', step);
    $sliderUpper.css('flex-grow', 1 - step);
  }

  /* End Copy Paste Internal Function from above */


  var $section = $(`#${sectionId}`);
  var $switchLabel = $section.find('label.mdl-switch').first();
  var $switch = $switchLabel.children().first();
  var $slider = $section.find('input.mdl-slider').first();
  var $sliderLower = $slider.next().children().first(); // .mdl-slider__background-lower
  var $sliderUpper = $sliderLower.next();               // .mdl-slider__background-upper

  var step = number2step[sceneNumber];
  _setSwitchUI($switchLabel, $switch, step > 0);
  _setSliderUI($slider, $sliderLower, $sliderUpper, step);
}
