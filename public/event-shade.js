'use strict';

/* globals __get */

function initShade(sectionId, zoneId) {

  var $section = $(`#${sectionId}`);

  $section.find(`#${sectionId}-up`).click(function(e) {
    __get(`/api/zone/${zoneId}/group/2/callScene/5`);
  });

  $section.find(`#${sectionId}-down`).click(function(e) {
    __get(`/api/zone/${zoneId}/group/2/callScene/0`);
  });

  $section.find(`#${sectionId}-preset1`).click(function(e) {
    __get(`/api/zone/${zoneId}/group/2/callScene/0`);
  });

  $section.find(`#${sectionId}-preset2`).click(function(e) {
    __get(`/api/zone/${zoneId}/group/2/callScene/19`);
  });

  $section.find(`#${sectionId}-preset3`).click(function(e) {
    __get(`/api/zone/${zoneId}/group/2/callScene/18`);
  });

  $section.find(`#${sectionId}-preset4`).click(function(e) {
    __get(`/api/zone/${zoneId}/group/2/callScene/17`);
  });

}
