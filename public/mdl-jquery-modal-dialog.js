function showLoading() {
    // remove existing loaders
    $('.loading-container').remove();
    $('<div id="orrsLoader" class="loading-container"><div><div class="mdl-spinner mdl-js-spinner is-active"></div></div></div>').appendTo("body");

    componentHandler.upgradeElements($('.mdl-spinner').get());
    setTimeout(function () {
        $('#orrsLoader').css({opacity: 1});
    }, 1);
}

function hideLoading() {
    $('#orrsLoader').css({opacity: 0});
    setTimeout(function () {
        $('#orrsLoader').remove();
    }, 400);
}

function showDialog(options) {
    options = $.extend({
        id: 'orrsDiag',
        title: null,
        text: null,
        negative: false,
        positive: false,
        cancelable: true,
        contentStyle: null,
        onLoaded: false
    }, options);

    // remove existing dialogs
    $('.dialog-container').remove();
    $(document).unbind("keyup.dialog");

      // | <div class="mdl-card mdl-shadow--2dp">
      // |   <div class="mdl-card__title">
      // |     <h2 class="mdl-card__title-text">Attention</h2>
      // |   </div>
      // |   <div class="mdl-card__supporting-text">
      // |     A window in the kitchen is still open.
      // |   </div>
      // |   <div class="mdl-card__actions" style="text-align:right">
      // |     <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">OK</button>
      // |   </div>
      // |   <div class="mdl-card__menu">
      // |     <i class="material-icons">info_outline</i>
      // |   </div>
      // | </div>
    $('<div id="' + options.id + '" class="dialog-container"><div class="mdl-card mdl-shadow--16dp" style="padding-bottom:16px"></div></div>').appendTo("body");
    var dialog = $('#orrsDiag');
    var content = dialog.find('.mdl-card');
    if (options.contentStyle != null) content.css(options.contentStyle);
    if (options.title != null) {
        $('<div class="mdl-card__title"><h2 class="mdl-card__title-text">' + options.title + '</h2></div>').appendTo(content);
    }
    if (options.text != null) {
        $('<div class="mdl-card__supporting-text" style="margin-left:0px;margin-right:0px">' + options.text + '</div>').appendTo(content);
    }
    if (options.negative || options.positive) {
        // var buttonBar = $('<div class="mdl-card__actions dialog-button-bar"></div>');
        var buttonBar = $('<div class="mdl-card__actions" style="padding-left:15px;padding-right:15px;border-top-width:0px;text-align:right"></div>');
        if (options.negative) {
            options.negative = $.extend({
                id: 'negative',
                title: 'Cancel',
                onClick: function () {
                    return false;
                }
            }, options.negative);
            var negButton = $('<button class="mdl-button mdl-js-button mdl-js-ripple-effect" id="' + options.negative.id + '">' + options.negative.title + '</button>');
            negButton.click(function (e) {
                e.preventDefault();
                if (!options.negative.onClick(e))
                    hideDialog(dialog)
            });
            negButton.appendTo(buttonBar);
        }
        if (options.positive) {
            options.positive = $.extend({
                id: 'positive',
                title: 'OK',
                onClick: function () {
                    return false;
                }
            }, options.positive);
                      // |     <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">OK</button>
            var posButton = $('<button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" id="' + options.positive.id + '">' + options.positive.title + '</button>');
            // var posButton = $('<button class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" id="' + options.positive.id + '">' + options.positive.title + '</button>');
            posButton.click(function (e) {
                e.preventDefault();
                if (!options.positive.onClick(e))
                    hideDialog(dialog)
            });
            posButton.appendTo(buttonBar);
        }
        buttonBar.appendTo(content);
    }
    $('<div class="mdl-card__menu"><i class="material-icons">info_outline</i></div>').appendTo(content);
    componentHandler.upgradeDom();
    if (options.cancelable) {
        dialog.click(function () {
            hideDialog(dialog);
        });
        $(document).bind("keyup.dialog", function (e) {
            if (e.which == 27)
                hideDialog(dialog);
        });
        content.click(function (e) {
            e.stopPropagation();
        });
    }
    setTimeout(function () {
        dialog.css({opacity: 1});
        if (options.onLoaded)
            options.onLoaded();
    }, 1);
    return dialog;
}

function hideDialog(dialog) {
    $(document).unbind("keyup.dialog");
    dialog.css({opacity: 0});
    setTimeout(function () {
        dialog.remove();
    }, 400);
}
