'use strict';

/* globals __get, showDialog */

var $sectionHome = $('#home-section-home');
var $sectionAway = $('#home-section-away');

// Home

function _setApartmentIsAway(isAway) {
  if (isAway) {
    $sectionHome.addClass('hidden');
    $sectionAway.removeClass('hidden');
  } else {
    $sectionAway.addClass('hidden');
    $sectionHome.removeClass('hidden');
  }
}

__get('api/apartment/getState', function(json) {
  console.log('api/apartment/getState', JSON.stringify(json));
  _setApartmentIsAway(json.result.apartment.state === 'away');
});

$('#home-section-home-btn').click(function() {
  __get('api/apartment/setState/away', function(json) {
    if (json.windowOpen) {

      showDialog({
        title: 'Attention',
        text: 'A window in the kitchen is still open.',
        positive: {
          title: 'OK',
          onClick: function (e) {
          }
        }
      });

    } else {
      _setApartmentIsAway(true);
    }
  });
});

$('#home-section-away-btn').click(function() {
  __get('api/apartment/setState/home', function(json) {
    _setApartmentIsAway(false);
  });
});


// http://www.jqueryscript.net/lightbox/jQuery-Modal-Dialog-Plugin-For-Material-Design-Lite.html
// showDialog({
//   title: 'Action',
//   text: 'Text to display',
//   cancelable: false,
//   negative: {
//     title: 'Nope'
//   },
//   positive: {
//     title: 'Yay',
//     onClick: function (e) {
//       alert('Action performed!');
//     }
//   }
// });
//
// // id of modal dialog
// id: 'orrsDiag',
// // modal title
// title: null,
// // text to display
// text: null,
// // for action buttons
// negative: false,
// positive: false,
// // set to false to make the modal dialog only be closed by using action buttons
// cancelable: true,
// // additional CSS styles appended to the modal
// contentStyle: null,
// // callback function
// onLoaded: false
