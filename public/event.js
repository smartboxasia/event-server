'use strict';

/* globals io, componentHandler, showDialog, hideDialog */
/* globals initLight, initAircond, initShade, syncLight */
/* globals _setApartmentIsAway */

$(document).ready(function() {

  /**
   * Web Socket
   */
  // var dialog;
  var host   = $('#server-host');
  var socket = io.connect(host);
      socket.on('socket-device', function (json) {
        $(json.id).text(json.percentage);
      });

      socket.on('socket-doorbird', function (json) {
        console.log('socket-doorbird', JSON.stringify(json));

        showDialog({
          title: 'Attention',
          text: 'Someone pressed your home doorbell',
          positive: {
            title: 'OK',
            onClick: function (e) {
              // alert('Action performed!');
            }
          }
        });

      });

      socket.on('socket-update-home', function (json) {
        console.log('socket-update-home', JSON.stringify(json));
        var isAway = json.apartment.state === 'away';
        _setApartmentIsAway(isAway);
      });

      socket.on('socket-update-light', function (json) {
        console.log('socket-update-light', JSON.stringify(json));

        var sectionId = json.sectionId;
        var zoneId2SectionId = {
          '26218': 'foyer-section-light',
          '1': 'living-section-light',
          '2': 'kitchen-section-light'
        };

        syncLight(zoneId2SectionId[sectionId], json.sceneNumber);

        if (sectionId === '26218') {
          syncLight('home-section-light', json.sceneNumber); // foyer=home, home=foyer
        }

      });

      socket.on('socket-alarm', function(json) {
        console.log('socket-alarm', JSON.stringify(json));

        showDialog({
          title: 'Attention',
          text: 'Home alarm is initiated!',
          positive: {
            title: 'OK',
            onClick: function (e) {
              // alert('Action performed!');
            }
          }
        });

      });



  /**
   * Get Server Status
   */
  componentHandler.upgradeAllRegistered();

  initLight('home-section-light', 26218, '303505d7f800004000037491');

  // Front Door (Foyer)

  var doorbirdIp = $('#doorbirdIp');
  $('#foyer-section-doorbird-opendoor').click(function() {
    $.get(`http://${doorbirdIp}/bha-api/open-door.cgi`);
  });

  initLight('foyer-section-light', 26218, '303505d7f800004000037491');

  // Living Room
  initLight('living-section-light', 1, '303505d7f8000040000374c5');  // TODO change dsid
  initShade('living-section-shade', 1);
  initAircond('living-section-aircond');

  // Kitchen
  initLight('kitchen-section-light', 2, '303505d7f8000040000372d3');
  initShade('kitchen-section-shade', 2);

});
