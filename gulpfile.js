'use strict';

var gulp       = require('gulp');
var nodemon    = require('gulp-nodemon');
var livereload = require('gulp-livereload');


gulp.task('develop', function() {
  livereload.listen();

  nodemon({
    script: 'app.js',
    ext: 'js pug css'
  }).on('restart', function() {
    setTimeout(function() {
      livereload.changed(__dirname);
    }, 1000);
  });

});

gulp.task('default', [ 'develop' ]);
