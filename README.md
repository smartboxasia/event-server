# Event Web Server

this repo is made to be used in events as a mobile app camouflage.

## Setting Up

1. Create Application Token

    ```
    curl -k -i "https://dss.lan:8080/json/system/requestApplicationToken?applicationName=[appName]"

    {"result":{"applicationToken":"[appToken]"},"ok":true}
    ```

2. Get Session Token (by Logging In)

    ```
    curl -k -i "https://dss.lan:8080/json/system/login?user=[username]&password=[password]"

    {"result":{"token":"[sessionToken]"},"ok":true}
    ```

3. Approve Application Token using Session Token

    ```
    curl -k -i "https://dss.lan:8080/json/system/enableToken?applicationToken=[appToken]&token=[token]"

    {"ok":true}
    ```


## Start (Development Mode)

```
npm run dev
```


## Start (Production Mode)

```
npm Start
```
