'use strict';

/**
 * Module dependencies.
 */

var debug  = require('debug')('event:server');
var http   = require('http');
var config = require('./config/config');

console.log(config);


/**
 * Create HTTP server.
 */
var bus    = require('./config/eventbus')();
var app    = require('./config/express')(config, bus);
var server = http.createServer(app);


/**
 * Socket.io Handling
 */

var count = 0;
var io = require('socket.io')(server);
      io.on('connection', function (socket) {
        console.log(`client connected. count: ${++count}`);

        socket.on('socket-device', function (json) {
          io.emit('socket-device', json);
        });

        socket.on('socket-doorbird', function (json) {
          io.emit('socket-doorbird', json);
        });

        socket.on('socket-update-home', function (json) {
          io.emit('socket-update-home', json);
        });


        socket.on('disconnect', function() {
          console.log(`client disconnected. count: ${--count}`);
        });
      });


/**
 * Bus Handling (route to socket.io)
 */

bus.subscribe('bus-doorbird', function(data) {
  io.emit('socket-doorbird', data);
});

bus.subscribe('bus-alarm', function(data) {
  io.emit('socket-alarm', data);
});

bus.subscribe('bus-update-home', function(data) {
  io.emit('socket-update-home', data);
});

bus.subscribe('bus-update-light', function(data) {
  io.emit('socket-update-light', data);
});




/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(config.port, '0.0.0.0');
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof config.port === 'string'
    ? 'Pipe ' + config.port
    : 'Port ' + config.port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
